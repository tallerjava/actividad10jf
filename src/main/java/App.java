
import java.io.IOException;

public class App {

    public static void main(String[] args) throws IOException {
        ServicioReporte sReporte = new ServicioReporte();
        Reporte reporte = sReporte.calcular();
        reporte.ordenarPorPrecio();
        System.out.println("=Paises======Montos===");
        for (Donacion e : reporte.getDonacionesPorPais()) {

            System.out.println(e.getPais() + " " + e.getMonto());
        }

        System.out.println("Total General: " + reporte.getTotal());
    }
}
