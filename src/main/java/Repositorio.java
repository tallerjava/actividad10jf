
import java.io.*;
import java.util.ArrayList;

public class Repositorio {

    private ArrayList<Donacion> lista = new ArrayList<>();
    private String ruta= "C:\\Users\\Joe\\Documents\\ProyectosJava\\Recaudaciones\\Listadepruebas.txt";

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }
    public void cargarArchivo() throws IOException {
        FileReader fichero = new FileReader(ruta);
        /* Se crea un fichero de lectura*/
        BufferedReader br = new BufferedReader(fichero);
        /* Se crea un "lector" de ficheros*/
        String linea;
        String paisTemp;
        String montoTemp;
        while ((linea = br.readLine()) != null) {
            paisTemp = linea.split("\\|")[1];
            montoTemp = linea.split("\\|")[6];
            montoTemp = montoTemp.replace("$", "");
            if (!montoTemp.equals("donacion")) {
                Double flotante = Double.parseDouble(montoTemp);
                lista.add(new Donacion(paisTemp, flotante));
            }
        }
    }

    public ArrayList<Donacion> getLista() {
        return lista;
    }
}
