
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;

public class ServicioReporte {

    private HashMap<String, Double> donacionesPorPais = new HashMap();
    private BigDecimal montoParcial, montoTotal, acumulador = BigDecimal.ZERO;
    double total = 0;
    Repositorio repo = new Repositorio();

    public Reporte calcular() throws IOException {
        repo.cargarArchivo();
        for (Donacion e : repo.getLista()) {
            if (donacionesPorPais.containsKey(e.getPais())) {
                montoTotal = new BigDecimal(e.getMonto());
                montoParcial = new BigDecimal(donacionesPorPais.get(e.getPais()));
                montoTotal = BigDecimal.valueOf(montoTotal.doubleValue()).add(BigDecimal.valueOf(montoParcial.doubleValue()));
                donacionesPorPais.put(e.getPais(), montoTotal.doubleValue());
            } else {
                donacionesPorPais.put(e.getPais(), e.getMonto());
            }
            acumulador = acumulador.add(new BigDecimal(e.getMonto()));
            total = acumulador.doubleValue();
        }
        return new Reporte(total, donacionesPorPais);
    }
}
