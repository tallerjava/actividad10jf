
import java.io.IOException;
import java.util.ArrayList;
import static java.util.Collections.list;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestRepositorio {

    @Test(expected = IOException.class)
    public void Calcular_rutaInvalida_errorIOException() throws IOException {
        Repositorio repo = new Repositorio();
        repo.setRuta("");
        repo.cargarArchivo();
    }

    @Test
    public void cargarArchivo_rutaValida_generacionDelListado() throws IOException {
        Repositorio repo = new Repositorio();
        repo.setRuta("C:\\Users\\Joe\\Documents\\ProyectosJava\\Recaudaciones\\Listadepruebas.txt");
        repo.cargarArchivo();
        assertNotNull(repo.getLista());
    }
}
