
import java.io.IOException;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestServicioReporte {

    private String resultadoObtenido="0.0";
    private String resultadoEsperado="0.0";

    public TestServicioReporte() {
    }

    @Test
    public void Calcular_listaVacia_mostrarTotalGeneral0() throws IOException {
        Repositorio repo = new Repositorio();
        ServicioReporte servicio = new ServicioReporte();
        servicio.repo.setRuta("C:\\Users\\Joe\\Documents\\ProyectosJava\\Recaudaciones\\Listadepruebasvacia.txt");
        Reporte reporte = servicio.calcular();
        resultadoObtenido = String.valueOf(reporte.getTotal());
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
    @Test
        public void Calcular_listaValido_mostrarTotalGenera() throws IOException {
        Repositorio repo = new Repositorio();
        ServicioReporte servicio = new ServicioReporte();
        servicio.repo.setRuta("C:\\Users\\Joe\\Documents\\ProyectosJava\\Recaudaciones\\Listadepruebas.txt");
        Reporte reporte = servicio.calcular();
        resultadoEsperado="14.51";
        resultadoObtenido = String.valueOf(reporte.getTotal());
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
}
